//using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Triton.Utilities {

#region History

// History:
//   3/23/15 - SD -	Updated implementation to use System.Diagnostics.Stopwatch to avoid security errors.

#endregion

/// <summary>
/// Summary description for MvcTimer.
/// </summary>
///	<author>Scott Dyke</author>
public class MvcTimer
{
	private Stopwatch stopwatch;


	public MvcTimer()
	{
		stopwatch = new Stopwatch();
	}


	/// <summary>
	/// Gets the total elapsed time measured by the current instance, in seconds.
	/// </summary>
	public double Time
	{
		get {
			return (double)stopwatch.ElapsedTicks / Stopwatch.Frequency;
		}
	}
	

	public bool IsRunning
	{
		get {
			return stopwatch.IsRunning;
		}
	}


	/// <summary>
	/// Starts, or resumes, measuring elapsed time for an interval.
	/// </summary>
	public void Start()
	{
		stopwatch.Start();
	}


	/// <summary>
	/// Stops measuring elapsed time for an interval.
	/// </summary>
	public void Stop()
	{
		stopwatch.Stop();
	}


	/// <summary>
	/// Stops time interval measurement and resets the elapsed time to zero.
	/// </summary>
	public void Reset()
	{
		stopwatch.Reset();
	}




	//private readonly long frequency;
	//private double accumulatedTime;
	//private long endTime;
	//private bool isRunning;
	//private long startTime;


	///// <summary>
	///// Construct a new <c>MvcTimer</c>.
	///// </summary>
	//public MvcTimer()
	//{
	//	//  get the frequency used by the performance timer
	//	QueryPerformanceFrequency(ref frequency);
	//}


	///// <summary>
	///// Gets the run time of the timer.  If the timer is still
	///// running (Stop() has not been called), it returns the 
	///// time since the timer was started.  If the timer has
	///// been stopped, it return the time is was running (from
	///// Start() to Stop()).
	///// </summary>
	//public double Time
	//{
	//	get {
	//		if (isRunning) {
	//			long nowTime = 0;
	//			QueryPerformanceCounter(ref nowTime);
	//			return (nowTime - startTime) * 1.0 / frequency;
	//		} else {
	//			return accumulatedTime;
	//		}
	//	}
	//}


	//[DllImport("kernel32.dll")]
	//private static extern short QueryPerformanceCounter(ref long x);


	//[DllImport("kernel32.dll")]
	//private static extern short QueryPerformanceFrequency(ref long x);


	///// <summary>
	///// Starts the timer running.
	///// </summary>
	//public void Start()
	//{
	//	//  get the current performance timer time
	//	QueryPerformanceCounter(ref startTime);
	//	//  set the flag so we know the timer is running
	//	isRunning = true;
	//}


	///// <summary>
	///// Stops the timer.
	///// </summary>
	//public void Stop()
	//{
	//	//  only stop the timer if it was previously started
	//	if (isRunning) {
	//		//  get the current performance timer time
	//		QueryPerformanceCounter(ref endTime);
	//		//  add the time since the timer was started
	//		//  to the accumulated time
	//		accumulatedTime += (endTime - startTime) * 1.0 / frequency;
	//		//  set the flag so we know the timer is no 
	//		//  longer running
	//		isRunning = false;
	//	}
	//}


	///// <summary>
	///// Resets the timer so it's accumulated time is reset.
	///// </summary>
	//public void Reset()
	//{
	//	accumulatedTime = 0.0;
	//}
}
}