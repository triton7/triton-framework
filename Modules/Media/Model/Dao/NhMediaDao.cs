﻿using System;
using System.Linq;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using Triton.Model;
using Triton.NHibernate.Model.Dao;
using Triton.NHibernate.Model.Dao.Support;
using Triton.Model.Dao;

namespace Triton.Media.Model.Dao {

#region History

// History:
//  3/16/2015	SD	Updated Find method to chunk the ID criteria if the list is long. The query will error
//					if there is a large number of parameters in the query.

#endregion

/// <summary>
/// Media DAO Class
/// </summary>
public class NhMediaDao : NHibernateBaseDao<Media>, IMediaDao
{
	protected const int	ID_CHUNK_SIZE	= 1000;


	#region IMediaDao Members

	/// <summary>
	/// summary
	/// </summary>
	/// <param name="id">integer</param>
	/// <returns></returns>
	public Media Get(
		int id)
	{
		return base.Get(id);
	}


	/// <summary>
	/// summary
	/// </summary>
	/// <param name="filter">byexample</param>
	/// <returns></returns>
	public SearchResult<Media> Find(
		MediaFilter filter)
	{
		List<Media> medias = new List<Media>();

		int totalMatches = 0;


				//  if there is only ID criteria and only 1 ID, can just call Get
		if ((filter.Ids != null) && (filter.Ids.Length == 1) && string.IsNullOrEmpty(filter.Name)) {
			Media media = Get(filter.Ids[0]);
			if (media != null) {
				medias.Add(media);
				totalMatches = 1;
			}

				//  more complicated filtering
		} else {
			ICriteria criteria = base.Session.CreateCriteria<Media>("Media");

					//  if there are no IDs in the filter or the count < the chunk size, just do the straight query
			if ((filter.Ids == null) || (filter.Ids.Length < ID_CHUNK_SIZE)) {
				if ((filter.Ids != null) && (filter.Ids.Length > 0)) {
					criteria = criteria.Add(Expression.In("Id", filter.Ids));
				}

				if (filter.Name != null) {
					criteria = criteria.Add(Expression.Eq("Name", filter.Name));
				}

				criteria = criteria.AddOrder(Order.Asc("SortOrder"));

				medias = SessionUtilities.GetItems<Media>(base.Session, criteria, new BaseFilter(), out totalMatches);

					//  if the number if IDs > the chunk size...
			} else {
						//  divide the ID list into chunks and query for each chunk separately, combining the results
						//  from each iteration
				for (int start = 0; filter.Ids.Length > start; start += ID_CHUNK_SIZE) {
					criteria = base.Session.CreateCriteria<Media>("Media");
							//  get the chunk of ID for the iteration
					int[] idChunk = filter.Ids.Skip(start).Take(ID_CHUNK_SIZE).ToArray();

							//  set up the criteria
					if ((filter.Ids != null) && (filter.Ids.Length > 0)) {
						criteria = criteria.Add(Expression.In("Id", idChunk));
					}

					if (filter.Name != null) {
						criteria = criteria.Add(Expression.Eq("Name", filter.Name));
					}

							//  execute the query
					int thisTotal = 0;
					List<Media> chunkMedia = SessionUtilities.GetItems<Media>(base.Session, criteria, new BaseFilter(), out thisTotal);

							//  add to the running totals
					medias.AddRange(chunkMedia);
					totalMatches += thisTotal;
				}

				medias.OrderBy(m => m.SortOrder);
			}
		}


		SearchResult<Media> results = new SearchResult<Media>(
				medias.ToArray(),
				filter,
				1,
				medias.Count,
				totalMatches);

		return results;
	}


	public MediaFilter GetFilter()
	{
		return new MediaFilter();
	}

	#endregion
}
}
