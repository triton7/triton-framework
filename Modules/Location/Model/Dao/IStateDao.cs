﻿using System.Collections.Generic;
using Triton.Model;

namespace Triton.Location.Model.Dao {

#region History

// History:
//    3/6/14 - SD - Added GetFilter and FindState.

#endregion

public interface IStateDao
{

	StateFilter GetFilter();

	State Get(int id);

	IList<State> Get(
		State example);

	SearchResult<State> FindState(
		StateFilter filter);

	void Save(State state);

	void Delete(State state);
}
}